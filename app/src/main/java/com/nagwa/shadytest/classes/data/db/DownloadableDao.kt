package com.nagwa.shadytest.classes.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.nagwa.shadytest.classes.data.models.beans.Downloadable

@Dao
interface DownloadableDao {
    @Query("SELECT id FROM downloadable")
    fun requestDownloadablesIds(): List<Int>

    @Insert
    fun insertDownloadable(downloadable: Downloadable): Long
}