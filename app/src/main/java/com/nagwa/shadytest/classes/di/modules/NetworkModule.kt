package com.nagwa.shadytest.classes.di.modules

import androidx.lifecycle.MutableLiveData
import com.nagwa.shadytest.classes.data.models.beans.Downloadable
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import io.reactivex.disposables.CompositeDisposable

@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {
    @Provides
    fun provideCompositeDisposable(): CompositeDisposable {
        return CompositeDisposable()
    }

    @Provides
    fun provideBooleanMutableLiveData(): MutableLiveData<Boolean> {
        return MutableLiveData()
    }

    @Provides
    fun provideDownloadablesMutableLiveData(): MutableLiveData<List<Downloadable>> {
        return MutableLiveData()
    }

    @Provides
    fun provideIntegerMutableLiveData(): MutableLiveData<Int> {
        return MutableLiveData()
    }

    @Provides
    fun provideLongMutableLiveData(): MutableLiveData<Long> {
        return MutableLiveData()
    }
}