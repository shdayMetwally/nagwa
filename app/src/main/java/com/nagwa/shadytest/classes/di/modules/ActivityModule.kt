package com.nagwa.shadytest.classes.di.modules

import com.nagwa.shadytest.classes.data.models.beans.Downloadable
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

@Module
@InstallIn(ActivityComponent::class)
class ActivityModule {

    @Provides
    fun provideDownloadables(): List<Downloadable> {
        return ArrayList()
    }

}

