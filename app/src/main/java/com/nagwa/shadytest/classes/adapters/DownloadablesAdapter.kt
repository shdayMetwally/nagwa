package com.nagwa.shadytest.classes.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.nagwa.shadytest.R
import com.nagwa.shadytest.classes.data.models.beans.Downloadable
import com.nagwa.shadytest.databinding.ListItemDownloadableBinding
import javax.inject.Inject

class DownloadablesAdapter @Inject constructor(private var data: List<Downloadable>) :
    RecyclerView.Adapter<DownloadablesAdapter.ViewHolder>() {


    lateinit var context: Context

    var onClickListener: OnClickListener? = null

    private var selectedItemPosition = -1
    private var isDownloading = false
    private var progress = 0


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.list_item_downloadable, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.ivImage.setImageResource(if (data[position].type == "PDF") R.drawable.pdf_holder else R.drawable.video_holder)
        holder.binding.tvName.text = data[position].name
        holder.binding.ivDownloaded.visibility =
            if (data[position].isDownloaded) VISIBLE else GONE

        if (position == selectedItemPosition) {
            holder.binding.pbDownloading.progress = progress
            holder.binding.ivChecked.visibility = if (!isDownloading) VISIBLE else GONE
            holder.binding.pbDownloading.visibility = if (isDownloading) VISIBLE else GONE
        }else{
            holder.binding.ivChecked.visibility = GONE
            holder.binding.pbDownloading.visibility = GONE
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun setData(data: List<Downloadable>) {
        this.data = data
        notifyDataSetChanged()
    }

    fun startDownloading() {
        isDownloading = true
        onClickListener?.onStartDownload(data[selectedItemPosition].id)
    }

    fun setProgress(progress: Int) {
        this.progress = progress
        if (progress == 100) {
            data[selectedItemPosition].isDownloaded = true
            onClickListener?.onDownloadFinished(data[selectedItemPosition])
        }
        notifyDataSetChanged()
    }

    fun setDownloaded(ids: List<Int>) {
        for (i in ids.indices) {
            for (j in data.indices) {
                if (ids[i] == data[j].id) {
                    data[j].isDownloaded = true
                    break
                }
            }
        }
        notifyDataSetChanged()
    }

    fun reset() {
        isDownloading = false
        selectedItemPosition = -1
        progress = 0
        notifyDataSetChanged()
    }

    interface OnClickListener {
        fun onItemClick(selectedItemsCount: Int)
        fun onStartDownload(id: Int)
        fun onDownloadFinished(downloadable: Downloadable)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val binding: ListItemDownloadableBinding = ListItemDownloadableBinding.bind(itemView)
        private fun onItemClick() {
            if (isDownloading) {
                Toast.makeText(
                    context,
                    context.getString(R.string.wait_for_current_items_to_be_downloaded),
                    Toast.LENGTH_LONG
                ).show()
                return
            }

            if (data[adapterPosition].isDownloaded) return

            selectedItemPosition = if (selectedItemPosition != adapterPosition) adapterPosition else -1

            onClickListener?.onItemClick(selectedItemPosition)

            notifyDataSetChanged()
        }

        init {
            itemView.setOnClickListener {
                onItemClick()
            }
        }
    }
}