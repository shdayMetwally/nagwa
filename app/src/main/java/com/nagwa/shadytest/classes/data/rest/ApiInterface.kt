package com.nagwa.shadytest.classes.data.rest

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.nagwa.shadytest.classes.data.models.beans.Downloadable
import dagger.hilt.android.qualifiers.ApplicationContext
import io.reactivex.Observable
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ApiInterface @Inject constructor(@ApplicationContext var context: Context) {
    fun requestDownloadables(): Observable<List<Downloadable>> {
        return Observable.fromCallable {
            val jsonFileString = jsonFromAssets
            val gson = Gson()
            gson.fromJson(jsonFileString, object : TypeToken<List<Downloadable>>() {}.type)
        }
    }

    private val jsonFromAssets: String
        get() {
            val jsonString: String
            jsonString = try {
                val `is` = context.assets.open("response.json")
                val size = `is`.available()
                val buffer = ByteArray(size)
                `is`.read(buffer)
                `is`.close()
                String(buffer, charset("UTF-8"))
            } catch (e: IOException) {
                e.printStackTrace()
                return ""
            }
            return jsonString
        }
}