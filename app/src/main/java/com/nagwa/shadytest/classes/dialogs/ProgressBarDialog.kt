package com.nagwa.shadytest.classes.dialogs

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.view.LayoutInflater
import android.view.Window
import android.widget.LinearLayout
import com.nagwa.shadytest.databinding.DialogProgressBarBinding
import dagger.hilt.android.qualifiers.ActivityContext
import javax.inject.Inject

class ProgressBarDialog @Inject constructor(@ActivityContext context: Context) : Dialog(context) {
    var mBinding: DialogProgressBarBinding

    init {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setCancelable(false)
        mBinding = DialogProgressBarBinding.inflate(LayoutInflater.from(context))
        setContentView(mBinding.root)
        val back = ColorDrawable(Color.TRANSPARENT)
        val inset = InsetDrawable(back, 32)
        window?.setBackgroundDrawable(inset)
        window?.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
    }
}