package com.nagwa.shadytest.classes.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.nagwa.shadytest.classes.data.models.beans.Downloadable

@Database(entities = [Downloadable::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    // The associated DAOs for the database
    abstract fun downloadableDao(): DownloadableDao
}