package com.nagwa.shadytest.classes.data

import com.nagwa.shadytest.classes.data.db.AppDatabase
import com.nagwa.shadytest.classes.data.models.beans.Downloadable
import com.nagwa.shadytest.classes.data.rest.ApiInterface
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Repository @Inject constructor(private val apiInterface: ApiInterface, private val db: AppDatabase) {


    fun requestDownloadables(): Observable<List<Downloadable>> {
        return apiInterface.requestDownloadables()
    }

    fun insertDownloadable(downloadable: Downloadable): Observable<Long> {
        return Observable.fromCallable { db.downloadableDao().insertDownloadable(downloadable) }
    }

    fun requestDownloadablesIds(): Observable<List<Int>> {
        return Observable.fromCallable { db.downloadableDao().requestDownloadablesIds() }
    }
}