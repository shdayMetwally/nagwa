package com.nagwa.shadytest.classes.di.modules

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.room.Room
import com.nagwa.shadytest.classes.data.db.AppDatabase
import com.nagwa.shadytest.classes.data.db.DownloadableDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DataBaseModule {
    @Provides
    fun provideIntegersMutableLiveData(): MutableLiveData<List<Int>> {
        return MutableLiveData()
    }

    companion object {
        @Provides
        @Singleton
        fun provideAppDatabase(application: Application): AppDatabase {
            return Room.databaseBuilder(application,
                    AppDatabase::class.java, "nagwa")
                    .fallbackToDestructiveMigration()
                    .build()
        }

        @Provides
        @Singleton
        fun provideDownloadableDao(appDatabase: AppDatabase): DownloadableDao {
            return appDatabase.downloadableDao()
        }
    }
}