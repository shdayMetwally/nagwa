package com.nagwa.shadytest.classes.data.models.beans

import androidx.room.Entity
import androidx.room.PrimaryKey
import lombok.Getter
import lombok.Setter

@Setter
@Getter
@Entity(tableName = "downloadable")
class Downloadable(@field:PrimaryKey val id: Int, val type: String, val url: String, val name: String) {
    @Transient
    var isDownloaded = false




}