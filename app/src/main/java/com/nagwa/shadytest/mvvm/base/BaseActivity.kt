package com.nagwa.shadytest.mvvm.base

import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import com.nagwa.shadytest.classes.dialogs.ProgressBarDialog
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
open class BaseActivity : AppCompatActivity() {
    @Inject
    lateinit var progressDialog: ProgressBarDialog
    fun onLoading(isLoading: Boolean) {
        hideKeyboard()
        if (isLoading) progressDialog.show() else progressDialog.dismiss()
    }

    fun hideKeyboard() {
        val imm = this.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        var view = this.currentFocus
        if (view == null) {
            view = View(this)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}