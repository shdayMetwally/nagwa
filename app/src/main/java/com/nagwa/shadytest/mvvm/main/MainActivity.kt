package com.nagwa.shadytest.mvvm.main

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.nagwa.shadytest.classes.adapters.DownloadablesAdapter
import com.nagwa.shadytest.classes.data.models.beans.Downloadable
import com.nagwa.shadytest.databinding.ActivityMainBinding
import com.nagwa.shadytest.mvvm.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : BaseActivity(), DownloadablesAdapter.OnClickListener {
    @Inject
    lateinit var downloadablesAdapter: DownloadablesAdapter
    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        viewModel.onLoadingMutableLiveData.observe(this, {
            onLoading(it)
        })
        viewModel.downloadablesIdsResponseMutableLiveData.observe(this, {
            downloadablesAdapter.setDownloaded(it)
        })
        viewModel.insertDownloadableResponseMutableLiveData.observe(this, {
            downloadablesAdapter.reset()
        })
        viewModel.progressResponseMutableLiveData.observe(this, {
            downloadablesAdapter.setProgress(it)
        })
        viewModel.downloadablesResponseMutableLiveData.observe(this, {
            downloadablesAdapter.setData(it)
            viewModel.loadDownloadablesIds()
        })

        binding.btnDownload.setOnClickListener {
            binding.btnDownload.visibility = View.GONE
            downloadablesAdapter.startDownloading()
        }

        binding.rvDownloadables.adapter = downloadablesAdapter
        downloadablesAdapter.onClickListener = this

        viewModel.requestDownloadables()

    }

    override fun onItemClick(selectedItemposition: Int) {
        binding.btnDownload.visibility = if (selectedItemposition != -1) View.VISIBLE else View.GONE
    }

    override fun onStartDownload(id: Int) {
        viewModel.requestDownload(id)
    }

    override fun onDownloadFinished(downloadable: Downloadable) {
        viewModel.insertDownloadableId(downloadable)
    }
}