package com.nagwa.shadytest.mvvm.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.nagwa.shadytest.classes.data.Repository
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.disposables.CompositeDisposable
import lombok.Getter
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket
import java.net.SocketAddress
import javax.inject.Inject

@Getter
@HiltViewModel
open class BaseViewModel @Inject constructor() : ViewModel() {
    @Inject
    lateinit var repository: Repository

    @Inject
    lateinit var compositeDisposable: CompositeDisposable

    @Inject
    lateinit var onLoadingMutableLiveData: MutableLiveData<Boolean>
    fun isInternetAvailable(`object`: Any): Boolean {
        return try {
            val timeoutMs = 5000
            val sock = Socket()
            val sockaddr: SocketAddress = InetSocketAddress("8.8.8.8", 53)
            sock.connect(sockaddr, timeoutMs)
            sock.close()
            true
        } catch (e: IOException) {
            onLoadingMutableLiveData.postValue(false)
            false
        }
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}