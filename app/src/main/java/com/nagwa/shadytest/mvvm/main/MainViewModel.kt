package com.nagwa.shadytest.mvvm.main

import androidx.lifecycle.MutableLiveData
import com.nagwa.shadytest.classes.data.models.beans.Downloadable
import com.nagwa.shadytest.mvvm.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import lombok.Getter
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@Getter
@HiltViewModel
class MainViewModel @Inject constructor() : BaseViewModel() {
    @Inject
    lateinit var downloadablesResponseMutableLiveData: MutableLiveData<List<Downloadable>>

    @Inject
    lateinit var downloadablesIdsResponseMutableLiveData: MutableLiveData<List<Int>>

    @Inject
    lateinit var progressResponseMutableLiveData: MutableLiveData<Int>

    @Inject
    lateinit var insertDownloadableResponseMutableLiveData: MutableLiveData<Long>
    fun requestDownloadables() {
        compositeDisposable.add(Observable.just("")
                .doOnNext { onLoadingMutableLiveData.setValue(true) }
                .observeOn(Schedulers.io())
                .takeWhile { `data`: String -> isInternetAvailable(`data`) }
                .flatMap { repository.requestDownloadables() }
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally { onLoadingMutableLiveData.setValue(false) }
                .subscribe({ response: List<Downloadable> -> downloadablesResponseMutableLiveData.setValue(response) }) { t: Throwable -> Timber.e(t) })
    }

    fun loadDownloadablesIds() {
        compositeDisposable.add(Observable.just("")
                .observeOn(Schedulers.io())
                .flatMap { repository.requestDownloadablesIds() }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response: List<Int> -> downloadablesIdsResponseMutableLiveData.setValue(response) }) { t: Throwable -> Timber.e(t) })
    }

    fun requestDownload(id: Int) {
        compositeDisposable.add(Observable.interval(100, TimeUnit.MILLISECONDS)
                .take(100)
                .map { v: Long -> v + 1 }
                .subscribe({ progress: Long -> progressResponseMutableLiveData.postValue(progress.toInt()) }) { t: Throwable -> Timber.e(t) })
    }

    fun insertDownloadableId(downloadable: Downloadable) {
        compositeDisposable.add(Observable.just(downloadable)
                .observeOn(Schedulers.io())
                .flatMap { data: Downloadable -> repository.insertDownloadable(data) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response: Long -> insertDownloadableResponseMutableLiveData.setValue(response) }) { t: Throwable -> Timber.e(t) })
    }
}